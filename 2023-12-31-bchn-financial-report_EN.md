BCHN Financial Report 2023-12-31
================================

The Bitcoin Cash Node (BCHN) project practices and promotes transparent
reporting.

Project funds are held in a 3-of-5 multi-signature wallet.

This wallet is used to receive [donations](https://bitcoincashnode.org/#donate)
and pay for project expenses (general funding of BCH Node operations,
including equipment and contracting).

All our spending transactions are on the Bitcoin Cash (BCH) blockchain
except when we dispose of non-BCH coins, received through airdrops or
other means, which we may convert into BCH.

We use open source wallet software ([Electron Cash](https://www.electroncash.org/))
and maintain our accounts using [plain text accounting](https://plaintextaccounting.org/)
in a [ledger file](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/bookkeeping/bchn.journal)
which is part of the public
[project management repository](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public)
on GitLab.

Based on this public ledger, the project can account for all its income and
expenses and produce financial reports using auditable open source software.

Please refer to the Links section at the bottom for links to previous
financial reports.


Project account details as of 2023-12-31
----------------------------------------

The statements below cover transactions up to and including on December 31, 2023
(more precisely, up to block 826,141).

2620.40438884 BCH was held by the project at the time of writing.

This represents an decrease in available funds of 568.52685304 BCH since the
previous report in December 2022.
The project remains in a good financial state.

Since project start, a total of 3147.56910101 BCH has been spent so far,
broken down as indicated.

                          assets:crypto
          0.13141780 ABC    bchabc
       2620.40438884 BCH    bitcoincash
       3147.56910101 BCH  expenses
       2638.55900700 BCH    development
         87.97578591 BCH      backporting
         40.54174199 BCH      bounty
          0.25000000 BCH        doc
          0.50000000 BCH        feature
          6.09334702 BCH        review
         11.00000000 BCH        security
          6.75000000 BCH        test
         15.94839497 BCH        website
         62.65460295 BCH      daa
       2406.37508226 BCH      general
          5.71632624 BCH      general verification
         35.29546765 BCH      website
          6.80363425 BCH        flipstarter
         20.54222098 BCH        general
          7.94961242 BCH        i18n
        249.75377220 BCH    external:verification:scaling
          7.98748784 BCH    infrastructure
          5.10000000 BCH    marketing
                            mining fees
          0.00971673 BCH      bch
          0.00705620 ABC      bchabc
        243.90447537 BCH    pr
        241.90447537 BCH      representative
          2.00000000 BCH      video
          2.25464187 BCH    translation
                          income
      -5603.27196254 BCH    donation
         -0.16663168 ABC    donation_replay
         -5.09999336 BCH    refund:marketing
      -2063.99958445 ABC  equity:abc_fork


Funds
-----

Currently BCHN funds derive entirely from donations.

The donations below include those made directly to our project donation
address and those forwarded from tips on this site.

A total of 5.85342797 BCH in new donations was received during this reporting period.

No further XEC (eCash) was received in this reporting period as a result of
replayed BCH donation transactions.

A total of 5608.3719559 BCH had been received in donations since the start
of the project in February 2020. About 17% of that was raised via the original Flipstarter campaign which [successfully completed](https://bitcoincashnode.org/en/newsroom/bchn-flipstarted) on 26 April 2020, yielding 978 BCH in funding for BCHN's initial [project proposal](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/raw/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1.pdf).
Thereafter some big anonymous donations have been received:

- 1000 BCH in August 2020
- 1000 BCH in January 2021
- 1000 BCH in February 2021
- 1000 BCH in November 2021
-  200 BCH in May 2022

The remainder was received from several business and personal donations.
Some of our biggest known donors include ASICseer.com, Marc de Mesel,
Georg Engelmann and 'zveda'.

We thank everyone who has generously donated to us during the time since
our last report.


Expenses
--------

Below are the project's expenses since the last overview on December 8, 2022.

They amount to 569.27903616 BCH over that period, or about 47.44 BCH / month
(compared to ~ 208 BCH / month over the previous report's period).

The mining fees have been excluded.

    2023-01-01 ex:development:general 55.67063697 BCH
    2023-01-01 ex:development:general  3.88601036 BCH
    2023-01-01 ex:pr:representative   12.95336788 BCH
    2023-01-01 ex:development:general 14.24870466 BCH
    2023-01-02 ex:development:general 12.56281400 BCH
    2023-02-01 ex:development:general  2.83961835 BCH
    2023-02-01 ex:development:general  9.46396123 BCH
    2023-02-01 ex:development:general 39.75164685 BCH
    2023-02-01 ex:pr:representative    9.50570342 BCH
    2023-02-08 ex:development:general  0.47348485 BCH
    2023-03-01 ex:development:general  3.73217884 BCH
    2023-03-01 ex:development:general 12.12777072 BCH
    2023-03-01 ex:development:general 55.98686174 BCH
    2023-03-01 ex:pr:representative    9.32835821 BCH
    2023-03-30 ex:development:general  7.74793388 BCH
    2023-03-31 ex:development:general 38.21399839 BCH
    2023-03-31 ex:development:general  7.99424414 BCH
    2023-04-02 ex:pr:representative   10.00000000 BCH
    2023-05-01 ex:development:general  2.13821416 BCH
    2023-05-01 ex:development:general  5.33504054 BCH
    2023-05-01 ex:development:general 53.35951506 BCH
    2023-05-03 ex:pr:representative   10.63829787 BCH
    2023-05-31 ex:development:general  1.11607140 BCH
    2023-05-31 ex:pr:representative   11.16071429 BCH
    2023-05-31 ex:development:general 36.47537359 BCH
    2023-07-01 ex:development:general  6.96864111 BCH
    2023-07-02 ex:pr:representative    4.29553265 BCH
    2023-08-01 ex:development:general  2.06296158 BCH
    2023-08-01 ex:pr:representative    5.14403292 BCH
    2023-08-31 ex:development:general  0.29223359 BCH
    2023-08-31 ex:pr:representative    6.03864734 BCH
    2023-08-31 ex:development:general 25.14247401 BCH
    2023-09-06 ex:development:general  3.25520833 BCH
    2023-10-01 ex:development:general 22.20624312 BCH
    2023-10-02 ex:pr:representative    5.12295082 BCH
    2023-11-01 ex:development:general 29.95867768 BCH
    2023-11-01 ex:development:general  7.26141080 BCH
    2023-11-01 ex:development:general  0.52098528 BCH
    2023-11-02 ex:pr:representative    5.35331910 BCH
    2023-12-01 ex:development:general  6.67735042 BCH
    2023-12-01 ex:development:general  6.67675598 BCH
    2023-12-01 ex:pr:representative    5.59109003 BCH

In the time since the last report, BCHN activity was directed towards:

- review and implementation of CHIPs and related application changes
  for the May 2024 Bitcoin Cash network upgrade
- bug fixing
- optimization
- improvement of tests and benchmarks
- code cleanup
- general backports
- user support

No bounties were awarded in this period, but a bounty process is
in development.

95.13201453 BCH were spent on user relations, covering twelve monthly
payments to our representative.

474.14702163 BCH were spent on client software development and maintenance.


Basic statistics about BCHN project ledger
------------------------------------------

These are derived from hledger tool's statistic report.

    Main file                : bchn.journal
    Included files           : prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk.journal
                               pp03e95qz7yene7v4vdx2vfsqx4qsq4wfvw8z3pkjf.journal
                               prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk_replays_to_abc.journal
    Transactions span        : 2020-02-22 to 2023-12-21 (1398 days)
    Last transaction         : 2023-12-20 (19 days ago)
    Transactions             : 2618 (1.9 per day)
    Transactions last 30 days: 2 (0.1 per day)
    Transactions last 7 days : 0 (0.0 per day)
    Payees/descriptions      : 1
    Accounts                 : 28 (depth 4)
    Commodities              : 3 (, ABC, BCH)


BCHA (XEC) policy
-----------------

Since converting its BCHA (now: XEC) holdings into BCH in November 2020,
the project holds virtually no more ABC / eCash coins except a minimal
amount which has been received since due to replaying of regular donations
made to our BCH wallet (the 'donation_replay' income position).

As income from XEC transactions has been dormant over this entire reporting
period, and our XEC coin holdings at this time still amount to less than 4 USD.

If there is no significant change in XEC holdings or income during the next
reporting period, the next report may omit XEC balances entirely due to being
negligible and not worth tracking in detail, especially as we do not use
the coin for payments.

As before, should we receive significant ABC coin donations, the project may
convert them to BCH as it does not intend to hold them.


Ceasing use of SLP
------------------

In the past, the project has used a SimpleLedger (SLP) token donation address
(please refer to previous financial reports).

With the advent of miner-validated CashToken on Bitcoin Cash, we have decided
to deprecate our SLP donation address and we will not attempt redeem any tokens
currently held there.

Only CashToken donations will be considered going forward.


CashTokens
----------

For those wishing to donate any tokens other than BCH to the project, we ask
that you only do so using CashTokens (not SLP, not tokens on SmartBCH).

The token-aware version of our main donation address is:

    bitcoincash:rrnc2exht3zxlrqqcat690tc85cvfuypngs5ruguy9

We gratefully acknowledge the receipt of some CashTokens which people have
kindly sent to us (including 100K DOGECASH, 0.01 WBCH, a BCH Guru NFT
and a Dragon Ball NFT).

We conduct our payments exclusively in Bitcoin Cash up to now, and that is
most directly practical and valuable to the project.

At this point, we do not intend to systematically report and track CashTokens
donated to us as they mostly remain dormant and we do not spend time exchanging
them for other assets.

We may do so in future for tokens that acquire significant value or utility to
the project, but not generally for all CashTokens sent our way, as this would
be a way to consume significant project resources.


Other sidechains / token systems
--------------------------------

The project does not at this time keep a SmartBCH account, or use other
tokens than the ones mentioned above.

We will keep you informed of developments and post any related information
should we decide to accept donations in other tokens in future.

---

Links
-----

Previous financial overviews:

 - [Financial overview in report of 14 March 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-03-14-84f2992d#financial-overview)

 - [Financial overview in report of 31 March 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-03-31-1e0a4c39#financial-overview)

 - [Financial overview in report of 15 April 2020](https://read.cash/@freetrader/bchn-lead-maintainer-report-2020-04-15-449fc115#financial-overview)

 - [BCHN Financial Report 2020-06-03](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-06-03-d2a0232f)

 - [BCHN Financial Report 2020-07-07](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-07-07-d04c7e65)

 - [BCHN Financial Report 2020-09-03](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-09-03-a477d4e2)

 - [BCHN Financial Report 2020-10-20](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-10-20-85ecb54c)

 - [BCHN Financial Report 2020-12-05](https://read.cash/@bitcoincashnode/bchn-financial-report-2020-12-05-db64dbdf)

 - [BCHN Financial Report 2021-02-08](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-02-08-ae9d834d)

 - [BCHN Financial Report 2021-04-17](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-04-17-d849b874)
 
 - [BCHN Financial Report 2021-06-11](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-06-11-b114bb7c)
 
 - [BCHN Financial Report 2021-11-24](https://read.cash/@bitcoincashnode/bchn-financial-report-2021-11-24-2fd7a1a4)

 - [BCHN Financial Report 2022-06-07](https://read.cash/@bitcoincashnode/bchn-financial-report-2022-06-07-64434232)

 - [BCHN Financial Report 2022-12-08](https://read.cash/@bitcoincashnode/bchn-financial-report-2022-12-08-90a59e3a)

Flipstarter funding proposal:

 - [English, v1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/raw/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1.pdf) (PDF)

 - [Chinese, v1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/funding/campaigns/2020-q2-flipstarter/pdf/Bitcoin_Cash_Node_Flipstarter_Funding_Proposal_v1_1_CN.pdf) (PDF)

 - [Spanish, 1.1](https://gitlab.com/bitcoin-cash-node/bchn-project-management/bchn-pm-public/-/blob/master/finance/funding/campaigns/2020-q2-flipstarter/proposal/proposal-es.mediawiki) (mediawiki)


Getting in touch with BCHN:

 - Website: https://bitcoincashnode.org

 - Development & support Slack chat invite link:
   https://join.slack.com/t/bitcoincashnode/shared_invite/zt-egg3c36d-2cglIrKcbnGpIQFaKFzCWA

 - Telegram: https://t.me/bitcoincashnode (there is a bridge channel to our Slack)

 - IRC channel: Join #bchnode on Freenode (we see messages on our Slack via an IRC bridge channel)

 - Logs of our development Slack: http://logs.bchnode.org/

 - Main development repository on GitLab:
   https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node

 - Easy download link via our website:
   https://bitcoincashnode.org/download.html

 - Release mirrors on Github:
   https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases

 - Donation address: https://bitcoincashnode.org/#donate

 - Follow us on Twitter: https://twitter.com/bitcoincashnode

 - Follow us on Reddit: https://www.reddit.com/r/bchnode
