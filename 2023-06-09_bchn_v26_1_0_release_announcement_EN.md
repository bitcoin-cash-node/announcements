June 9, 2023

# Release announcement: Bitcoin Cash Node v26.1.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its minor release
version 26.1.0.

This release implements a number of corrections and performance improvements,
It also includes some interface extensions, such as new fields in `getpeerinfo`
RPC call and new syntax variable for `-walletnotify`.

Users who are running any of our previous releases are recommended to upgrade
to v26.1.0,

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v26.1.0

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
