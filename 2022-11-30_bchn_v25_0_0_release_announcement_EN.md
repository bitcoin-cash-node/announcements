November 30, 2022

# Release announcement: Bitcoin Cash Node v25.0.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its major release
version 25.0.0.

This release implements the May 15, 2023 network upgrade.

It delivers four Cash Improvement Proposals that are consensus changes,

- CHIP-2021-01 Restrict Transaction Version (v1.0)
- CHIP-2021-01 Minimum Transaction Size (v0.4)
- CHIP-2022-02 CashTokens (v2.2.1)
- CHIP-2022-05 P2SH32 (v1.5.1)

and a number of other enhancements, bugfixes and performance improvements.

BCHN users should consider an update prior to May 15, 2023 as mandatory.
The v24.x.0 software will expire on May 15, 2023, and will start to warn
of the need to update ahead of time, from April 15, 2023 onward.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v25.0.0

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

For more information about the May 15, 2023 network upgrade, visit

  https://upgradespecs.bitcoincashnode.org/2023-05-15-upgrade/

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
