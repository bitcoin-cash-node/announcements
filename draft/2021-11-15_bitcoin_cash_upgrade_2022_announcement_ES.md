15 de noviembre de 2021

# Actualización de Bitcoin Cash 2022

El 15 de mayo de 2022, Bitcoin Cash se someterá a una actualización técnica para mejorar
sus capacidades de contratación de máquinas virtuales (VM).
La actualización permitirá una variedad de nuevos productos y servicios financieros al:

1. Permitir operaciones aritméticas en números significativamente mayores.
   Un rango aritmético más amplio permitirá que los contratos gestionen grandes
   balances con total precisión, permitiendo tesorerías basadas en contratos y
   mejorando la eficiencia en el diseño de contratos existentes.
   
2. Agregar nuevas operaciones, incluidas las operaciones de introspección nativas. Estas
   nuevas operaciones permitirán el desarrollo de carteras mucho más seguras,
   pagos recurrentes más eficientes, entre otros beneficios.

Esta actualización ha sido discutida, desarrollada y revisada por un gran
consorcio de empresas, equipos de desarrollo y desarrolladores independientes -
incluidos todos los equipos de software de nodo existentes. Al 15 de noviembre de 2021,
la deliberación ha concluido sin controversia, y la actualización se activará
el 15 de mayo de 2022 sin una división de la red.

El software Bitcoin Cash Node (BCHN) incluirá soporte para la
Actualización de Bitcoin Cash 2022 en su próxima versión v24.0.0.

## Cronograma de actualización

* 15 de septiembre de 2021: la actualización se activó en una red de prueba pública.
* 15 de noviembre de 2021: BCHN se compromete a actualizar el alcance y a una versión v24.0.0 en noviembre.
* 15 de febrero de 2022: se espera que todo el software del ecosistema tenga una
versión que soporte esta actualización.
* 15 de mayo de 2022: la actualización se activará en la red principal.

## Preparación para la actualización

Para los usuarios de Bitcoin Cash, esta actualización no requiere preparación; los pagos
se pueden realizar y aceptar de forma segura durante la activación. Los programas de Billetera
existente seguirá funcionando sin actualizaciones.

Se recomienda a los mineros, casas de intercambios y otros operadores de nodos que actualicen el
programa del nodo antes del 15 de mayo de 2022 para evitar interrupciones en el servicio. La actualización
no requiere tiempo de inactividad y las operaciones pueden continuar con normalidad.

## Detalles técnicos

La actualización del 15 de mayo de 2022 incluye los siguientes cambios en el consenso:

- [CHIP-2021-03: números enteros de script más grandes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Bigger-Script-Integers.md)
  [(Discusión)](https://bitcoincashresearch.org/t/chip-2021-03-bigger-script-integers/39)
- [CHIP-2021-02: códigos de operación de introspección nativos](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md)
  [(Discusión)](https://bitcoincashresearch.org/t/chip-2021-02-add-native-introspection-opcodes/307)

Para obtener más información relacionada con BCHN sobre la actualización de la red del 15 de mayo de 2021,
por favor visita

  https://upgradespecs.bitcoincashnode.org/2022-05-15-upgrade/

Atentamente,

El equipo de Bitcoin Cash Node. 
