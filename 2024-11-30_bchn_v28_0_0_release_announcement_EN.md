November 30th, 2024

# Release announcement: Bitcoin Cash Node v28.0.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its major release
version 28.0.0.

This release implements the May 15, 2025 network upgrade.

It delivers two consensus change CHIPs that have gained wide acceptance:

- CHIP-2021-05 VM Limits: Targeted Virtual Machine Limits (git hash `b5b6c275` of 21 Nov 2024)
- CHIP-2024-07 BigInt: High-Precision Arithmetic for Bitcoin Cash (git hash `697725bc` of 21 Nov 2024)

and a number of other enhancements, bugfixes and performance improvements.

BCHN users should consider an update prior to May 15, 2025 as mandatory.
The v27.x.0 software will expire on May 15, 2025, and will start
to warn of the need to update ahead of time, from April 15, 2025 onward.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v28.0.0

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

For more information about the May 15, 2025 network upgrade, visit

  https://upgradespecs.bitcoincashnode.org/2025-05-15-upgrade/

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
