2024年11月30日

# BCHN v28.0.0版本发布公告

Bitcoin Cash Node (BCHN) 项目在此宣布重大版本更新 28.0.0。

此版本实行了2025年5月15号网络升级规格，包括以下两项已获得广泛支持的共识修改方案:

- CHIP-2021-05 VM Limits: Targeted Virtual Machine Limits (2024年11月21日 git hash `b5b6c275`)
- CHIP-2024-07 BigInt: High-Precision Arithmetic for Bitcoin Cash (2024年11月21日  git hash `697725bc`)

以及其他除错和性能提升。

BCHN用户在2025年5月15日前必须升级到此版本，
v27.x.0及以前的软件将于5月15日到期而失去功能，
并将于4月15日起开始警告用户。

访问以下网址，查看完整版版本说明：

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v28.0.0

该链接中有可执行程序和支持平台的源码，你也可以在BCHN的项目网页进行下载：

  https://bitcoincashnode.org

如需了解更多2025年5月15号升级的信息，请访问以下链接：

  https://upgradespecs.bitcoincashnode.org/2025-05-15-upgrade/

我们希望最新版本能带给你好的体验。让我们一起改善BCH。

BCHN团队